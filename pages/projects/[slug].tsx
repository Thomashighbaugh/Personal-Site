import fs from 'fs';
import matter from 'gray-matter';
import mdxPrism from 'mdx-prism';
import { GetStaticPaths, GetStaticProps } from 'next';
import { MDXRemote, MDXRemoteSerializeResult } from 'next-mdx-remote';
import { serialize } from 'next-mdx-remote/serialize';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import path from 'path';
import React from 'react';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeSlug from 'rehype-slug';
import Layout, { WEBSITE_HOST_URL } from '../../components/Layout';
import { MetaProps } from '../../types/layout';
import { ProjectType } from '../../types/projects';
import { projectFilePaths, PROJECTS_PATH } from '../../utils/mdxProjects';

// Custom components/renderers to pass to MDX.
// Since the MDX files aren't loaded by webpack, they have no knowledge of how
// to handle import statements. Instead, you must include components in scope
// here.
const components = {
  Head,
  Image,
  Link,
};

type ProjectPageProps = {
  source: MDXRemoteSerializeResult;
  frontMatter: ProjectType;
};

const ProjectPage = ({
  source,
  frontMatter,
}: ProjectPageProps): JSX.Element => {
  const customMeta: MetaProps = {
    title: `${frontMatter.title} - Thomas Leon Highbaugh`,
    description: frontMatter.description,
    image: `${WEBSITE_HOST_URL}${frontMatter.image}`,
    date: frontMatter.date,
    type: 'article',
    siteUrl: frontMatter.siteUrl,
    github: frontMatter.github,
  };
  return (
    <Layout customMeta={customMeta}>
      <article className="max-w-full  justify-center block">
        <h1 className="mb-3 text-gray-700 dark:text-gray-50">
          {frontMatter.title}
        </h1>
        <div className="prose dark:prose-dark">
          <MDXRemote {...source} components={components} />
          <iframe src={frontMatter.siteUrl} height="800" width="1388"  />
        </div>
      </article>
    </Layout> 
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const ProjectFilePath = path.join(PROJECTS_PATH, `${params.slug}.mdx`);
  const source = fs.readFileSync(ProjectFilePath);

  const { content, data } = matter(source);

  const mdxSource = await serialize(content, {
    // Optionally pass remark/rehype plugins
    mdxOptions: {
      remarkPlugins: [require('remark-code-titles')],
      rehypePlugins: [mdxPrism, rehypeSlug, rehypeAutolinkHeadings],
    },
    scope: data,
  });

  return {
    props: {
      source: mdxSource,
      frontMatter: data,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = projectFilePaths
    // Remove file extensions for page paths
    .map((path) => path.replace(/\.mdx?$/, ''))
    // Map the path into the static paths object required by Next.js
    .map((slug) => ({ params: { slug } }));

  return {
    paths,
    fallback: false,
  };
};

export default ProjectPage;
