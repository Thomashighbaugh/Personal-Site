export type ProjectType = {
  date?: string;
  description?: string;
  image?: string;
  slug: string;
  title: string;
  github: string;
  siteUrl: string;
  category: string;
};
